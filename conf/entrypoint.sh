#!/usr/bin/env bash

if [ ! -f /var/www/html/composer.json ]; then
    echo 'Starting Magento Download ..';
    mkdir /var/www/.composer;
    mv /auth.json /var/www/.composer/auth.json;

    sed -i -e "s/MAGENTO_PUBLIC_KEY/$MAGENTO_PUBLIC_KEY/g" /var/www/.composer/auth.json
    sed -i -e "s/MAGENTO_PRIVATE_KEY/$MAGENTO_PRIVATE_KEY/g" /var/www/.composer/auth.json

    mkdir /var/www/magento2;
    COMPOSER_HOME=/var/www/.composer/ composer create-project --repository-url=https://repo.magento.com/ magento/project-$MAGENTO_EDITION-edition:$MAGENTO_VERSION /var/www/magento2;
    cp -r /var/www/magento2/. /var/www/html;
    rm -rf /var/www/magento2;
    cp /var/www/.composer/auth.json /var/www/html/auth.json;

fi
if [ ! -f /var/www/html/app/etc/env.php ]; then
    echo 'Magento has not been installed';

    sleep 10;
    mysql --host="$DB_HOST" -u root -p"$MYSQL_ROOT_PASSWORD" -e "create database IF NOT EXISTS $DB_NAME";

    php /var/www/html/bin/magento setup:install --admin-firstname="$ADMIN_FIRSTNAME" --admin-lastname="$ADMIN_LASTNAME" --admin-email="$ADMIN_EMAIL" --admin-user="$ADMIN_USER" --admin-password="$ADMIN_PASSWORD" --backend-frontname="$ADMIN_BACKEND" --db-host="$DB_HOST" --db-name="$DB_NAME" --db-user=root --db-password="$MYSQL_ROOT_PASSWORD" --use-rewrites=1 --admin-use-security-key=0

fi

chown -R www-data:www-data /var/www/html;

if [ $SAMPLE_DATA = 1 ]; then
    php -d memory_limit=1024M /var/www/html/bin/magento sampledata:deploy;
    php -d memory_limit=1024M /var/www/html/bin/magento setup:upgrade;
    php -d memory_limit=1024M /var/www/html/bin/magento setup:di:compile;
    php -d memory_limit=1024M /var/www/html/bin/magento cache:flush;
    chown -R www-data:www-data /var/www/html;
fi


supervisord -n -c /etc/supervisord.conf
